build:
	docker build -t image_service_cli .

run:
	docker run \
		-e MICRO_REGISTRY=mdns \
		image_service_cli